# SimplePWA

Simple app for making PWA's.

## Getting started

clone the project:  
```
git clone https://gitlab.com/magnusmj/simplepwa.git  
```

install dependencies
### Ubuntu
```
sudo apt install python3-pyqt5  
sudo apt install python3-pyqt5.qtwebengine
```
### Fedora
```
sudo dnf install PyQt5  
sudo dnf install python3-qt5-webengine
```

## Run 
```
python3 pwa.py location="https://your.pwa.address"
```
