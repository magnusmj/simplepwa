#!/usr/bin/python3

import sys
from PyQt5.QtWidgets import QApplication
from GUI.MainWindow import MainWindow

app = QApplication(sys.argv)
debug = 'debug' in sys.argv
location = "https://www.google.com"
for arg in sys.argv:
	if 'location' in arg.lower() and '=' in arg.lower():
		location = arg.split("=")[1]
		print("location is: " + str(location))
	elif "https://" in arg.lower():
		location = arg
	elif "www." in arg.lower():
		location = "https://" + arg

mw = MainWindow(location)
mw.show()  # fullscreen On release

sys.exit(app.exec())
