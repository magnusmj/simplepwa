from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineWidgets import QWebEngineView


class WebView(QWebEngineView):
	def __init__(self, parent, location):
		QWebEngineView.__init__(self, parent)
		self.icon = None
		self.parent = parent
		self.address = location
		self.load(QUrl(self.address))
		self.loadFinished.connect(self.loaded)
		self.iconChanged.connect(self.icon_changed)

	def loaded(self):
		self.parent.setWindowTitle(self.title())

	def icon_changed(self, icon):
		self.icon = icon
		self.parent.setWindowIcon(icon);
		self.address = str(self.url().toString())
