from PyQt5.QtCore import QEvent, Qt, QUrl, QMargins, QByteArray, QBuffer, QIODevice, QSize
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QInputDialog, QLineEdit, QMessageBox
from pathlib import Path
from GUI.WebView import WebView
import GUI
import hashlib

class MainWindow(QWidget):
	def __init__(self, location):
		QWidget.__init__(self, None)
		self.setWindowTitle("SimplePWA")
		self.location = location
		self.setMinimumWidth(640)
		self.setMinimumHeight(480)
		self.web_view = WebView(self, location)

		layout = QVBoxLayout()
		self.setLayout(layout)
		layout.setContentsMargins(QMargins(0, 0, 0, 0))
		layout.addWidget(self.web_view)
		self.installEventFilter(self)
		self.closeNow = False

	def eventFilter(self, obj, event):
		if event.type() == QEvent.KeyRelease:
			if event.key() == Qt.Key_F5:
				print("refresh")
				self.web_view.load(QUrl(self.web_view.address))
				return True
			if event.key() == Qt.Key_F10:
				self.on_change_location()
				return True
			if event.key() == Qt.Key_F8:
				self.on_create_launcher()
				return True
		return False

	def on_change_location(self):
		self.web_view.address, ok = QInputDialog().getText(self, "Change location", "New address:", QLineEdit.Normal, self.web_view.address)
		if not "https://" in self.web_view.address:
			self.web_view.address = "https://" + self.web_view.address
		if ok and self.web_view.address:
			self.web_view.load(QUrl(self.web_view.address))

	def on_create_launcher(self):
		print("Create launcher " + GUI.cwd)
		hash_object = hashlib.md5(str(self.web_view.address).encode('utf-8'))
		hash = hash_object.hexdigest()
		icon_filename = str(Path.home()) + "/.local/share/simplepwa/" + hash + ".png"
		filename = str(Path.home()) + "/.local/share/applications/" + hash + '.desktop'
		launcher_content = "[Desktop Entry]"
		launcher_content += "\nName=" + self.windowTitle()
		launcher_content += "\nExec=python3 pwa.py location=" + self.web_view.address
		launcher_content += "\nType=Application"
		launcher_content += "\nTerminal=false"
		launcher_content += "\nPath=" + GUI.cwd
		launcher_content += "\nIcon=" + icon_filename
		#print(launcher_content)
		icon = self.web_view.icon
		self.save_icon_to_png(icon, icon_filename)
		with open(filename, 'w') as f:
			f.write(launcher_content)
		msg = QMessageBox(self)
		msg.setIcon(QMessageBox.Information)
		msg.setText("A launcher has been added")
		msg.setInformativeText("to " + self.windowTitle())
		msg.setWindowTitle("Launcher created")
		msg.setStandardButtons(QMessageBox.Ok )
		retval = msg.exec_()


	def save_icon_to_png(self, icon, filename, size=None, fmt='PNG'):
		if size is None:
			size = icon.availableSizes()[0]
			size = QSize(64,64)
		pixmap = icon.pixmap(size)
		array = QByteArray()
		buffer = QBuffer(array)
		buffer.open(QIODevice.WriteOnly)
		pixmap.save(buffer, fmt)
		buffer.close()
		with open(filename, 'wb') as f:
			f.write(array.data())

	def closeEvent(self, event):
		event.accept()

