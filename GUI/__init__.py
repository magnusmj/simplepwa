import os
from pathlib import Path

cwd = os.getcwd()
path = str(Path.home()) + "/.local/share/simplepwa/"
isExist = os.path.exists(path)
if not isExist:
    os.makedirs(path)